enum read_status
{
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 5,
    READ_INVALID_HEADER = 7
};


enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR = 1
};
