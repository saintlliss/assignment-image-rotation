#include "../include/image.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>


void rotate_image(struct image *input_image, struct image *output_image, size_t (get_rotated_in_some_direction_pixel_number) (size_t, size_t, struct image *));
size_t get_rotated_ccw_pixel_number(size_t width, size_t height, struct image *image);
size_t get_pixel_number(size_t width, size_t height, struct image *image);
