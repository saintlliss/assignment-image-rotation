#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/status_enums.h"
#include <stdio.h>
#include <stdlib.h>

void image_free(struct image *image_to_free);
// void rotate_image(struct image *input_image, struct image *output_image, size_t (get_rotated_in_some_direction_pixel_number) (size_t, size_t, struct image *));
enum read_status read_header(FILE *input, struct bmp_header* input_image_header);
struct image create_image(uint64_t width, uint64_t height);
enum read_status read_image(struct image *input_image, FILE *input);
enum write_status write_header(struct bmp_header header, FILE* output);
enum write_status write_image(struct image * output_image, FILE* output);


