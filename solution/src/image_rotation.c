#include "../include/image_rotation.h"

size_t get_pixel_number(size_t width, size_t height, struct image *image){
    return (width * image->height + height);
}

size_t get_rotated_ccw_pixel_number(size_t width, size_t height, struct image *image){
    return (((image->height * image->width) - (image->width * (height + 1))) + width);
}

void rotate_image(struct image *input_image, struct image *output_image, size_t (get_rotated_in_some_direction_pixel_number) (size_t, size_t, struct image *))
{
    uint64_t width, height;
    width = input_image->width;
    height = input_image->height;
    for (size_t i = 0; i < width; ++i)
    {
        for (size_t j = 0; j < height; ++j)
        {
            output_image->data[get_pixel_number(i, j, input_image)] = input_image->data[get_rotated_in_some_direction_pixel_number(i, j, input_image)];
        }
    }
}

