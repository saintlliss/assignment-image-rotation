#include "../include/util.h"
#include "../include/image_rotation.h"
#include <inttypes.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if (argc != 3){
        fprintf(stderr, "Not enought arguments provided\n");
        return 1;
    }
    FILE *input, *output;
    input = fopen(argv[1], "rb");
    output = fopen(argv[2], "wb");
    
    struct bmp_header input_image_header = {0};
    if (read_header(input, &input_image_header) != READ_OK){
        fprintf(stderr, "Unable to read image header\n");
        return READ_INVALID_HEADER;
    }

    uint64_t width = input_image_header.biWidth;
    uint64_t height = input_image_header.biHeight;

    struct image input_image = create_image(width, height);
    struct image output_image = create_image(height, width);

    if (read_image(&input_image, input)){
        fprintf(stderr, "Read error");
        return READ_INVALID_SIGNATURE;
    }

    input_image_header.biHeight = width;
    input_image_header.biWidth = height;

    rotate_image(&input_image, &output_image, get_rotated_ccw_pixel_number);

    if (write_header(input_image_header, output)){
        fprintf(stderr, "Write error");
        return WRITE_ERROR;
    }

    if (write_image(&output_image, output)){
        fprintf(stderr, "Write error");
    }

    image_free(&output_image);
    image_free(&input_image);

    return 0;
}
