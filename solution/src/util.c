#include "../include/util.h"

void image_free(struct image *image_to_free)
{
    if (image_to_free->data)
        free(image_to_free->data);
}

enum read_status read_header(FILE *input, struct bmp_header* input_image_header)
{
    if(fread(input_image_header, sizeof(struct bmp_header), 1, input))
        return READ_OK;
    return READ_INVALID_HEADER;
}

struct image create_image(uint64_t width, uint64_t height)
{
    struct image new_image = {0};
    new_image.height = height;
    new_image.width = width;
    new_image.data = malloc(sizeof(struct pixel) * width * height);
    return new_image;
}

enum read_status read_image(struct image *input_image, FILE *input)
{
    uint64_t width = input_image->width;
    uint64_t height = input_image->height;
    for (size_t i = 0; i < height; i++)
    {
        if (!fread(&(input_image->data[i * width]), sizeof(struct pixel), width, input))
        {
            image_free(input_image);
            return READ_INVALID_SIGNATURE;
        }
        if (fseek(input, (uint8_t)width % 4, SEEK_CUR))
        {
            image_free(input_image);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}

enum write_status write_header(struct bmp_header header, FILE *output)
{
    if (fwrite(&header, sizeof(struct bmp_header), 1, output) < 1)
    {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status write_image(struct image *output_image, FILE *output)
{
    uint64_t width = output_image->width;
    uint64_t height = output_image->height;
    size_t fill = 0;
    for (size_t i = 0; i < height; ++i)
    {
        if (fwrite(&(output_image->data[i * width]), sizeof(struct pixel), width, output) < width)
        {
            return WRITE_ERROR;
        }
        if (fwrite(&fill, 1, width % 4, output) < width % 4)
        {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

